; HowNow Document Converter

#define MyAppName "HowNow Document Converter"
#define MyAppVersion "10.0.0.1"
#define MyAppPublisher "Business Fitness"
#define MyAppURL "https://www.businessfitness.com.au"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{3AE8DB4E-59E1-4A64-82BB-47C067F253D6}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={commonappdata}\Business Fitness\{#MyAppName}
DisableDirPage=yes
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
CreateAppDir=true
OutputBaseFilename=HowNow Document Converter Setup
OutputDir=.\Output\
SetupIconFile=.\HowNow.ico
Compression=lzma
SolidCompression=yes
ArchitecturesInstallIn64BitMode=x64 ia64
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
VersionInfoCopyright=2018
VersionInfoProductName={#MyAppName}
DisableReadyPage=true
DisableStartupPrompt=true
DisableFinishedPage=true

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Files]
Source: ".\Source\*"; DestDir: "{app}\Source"; Flags: ignoreversion recursesubdirs createallsubdirs deleteafterinstall
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[INI]
Filename: {app}\Install.ini; Section: "HowNow Document Converter"; Key: "Setup Version"; Flags: uninsdeletesectionifempty; String: {#MyAppVersion};

[Run]
Filename: "{app}\Source\Setup.exe"; Parameters: "-s -d""{app}"""
Filename: "{app}\Source\gs925w32.exe"; Parameters: "/S"

[InstallDelete]
Name: "{src}\HowNowDCSetup.exe"; Type: files;

[UninstallDelete]
Name: {app}\Install.ini; Type: files

[UninstallRun]
Filename: "{app}\unInstpw64.exe"; Parameters: /uninstall /s
Filename: "{app}\unInstpw.exe"; Parameters: "-s""{app}"""



 
                         




